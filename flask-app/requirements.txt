flask>=2.0.2
flask_socketio>=5.1.1
eventlet==0.30.2
gunicorn>=20.1.0