// start WebSocket
var socket = io.connect('/');
let connected = false;
// Klaar met laden
// window.onload = (event) => {
// };

socket.on('connect', function() {
    // stuur socket_connect naar backend
    socket.emit('socket_connect');        
}); 

setInterval(() => {
    if (connected)
        socket.emit('getTime');
}, 100);

setInterval(() => {
    if (!connected && socket.connected){
        socket.emit('socket_connect');
        console.log('retry')
    }
}, 3000);

// functie secondes naar tijd String
function sec2time(timeInSeconds) {
    return new Date(timeInSeconds * 1000).toISOString().substring(11,19);
}

// krijg connected van server, client is verbonden met de server
socket.on('connected', () => {
    connected = true;
    console.log('Websocket connected');
    document.getElementById('state').textContent = 'connected';
    document.getElementById('state').style.color = 'green';
});

socket.on('time', (time) => {
    document.getElementById('time').textContent = sec2time(time  - (new Date().getTimezoneOffset() * 60));
});