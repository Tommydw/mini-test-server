from flask import Flask
from flask_socketio import SocketIO
app = Flask(__name__, template_folder='templates')

socket_ = SocketIO(app)
socket_.init_app(app, cors_allowed_origins="*")



from files import routes