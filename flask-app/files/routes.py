from socket import socket
from flask import render_template
from files import app, socket_
from flask_socketio import  emit
import time

@app.route('/')
def hello():
    return render_template("test.html")

# error handeling
# 404
@app.errorhandler(404)
def page_not_found(error):
    return render_template('error_page.html')

# krijg socket_connect van client
@socket_.on('socket_connect')
def socket_connect():
    emit('connected')
    return

@socket_.on('getTime')
def getTime():
    emit('time', time.time())
    return
