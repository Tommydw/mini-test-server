# Mini test server
Run `docker-build-images.yml` to build images:
```
docker-compose -f "docker-build-images.yml" up -d --build
```
* Dockerfile in nginx-app
* Dockerfile in flask-app

Run `docker-compose.yml` to run service
```
docker-compose -f "docker-compose.yml" up -d --build
```

# Docker 
export image: 
```
docker save flask-app:latest -o flask-app.tar && docker load -i flask-app.tar
docker save nginx-app:latest -o nginx-app.tar && docker load -i nginx-app.tar
```

import image:
```
docker image import flask-app.tar
docker image import nginx-app.tar
```